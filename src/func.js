const { listOfPosts } = require("./posts");

const getSum = (str1, str2) => {
	if(typeof str1 !== 'string' || typeof str2 !== 'string' || Number.isNaN(+str1) || Number.isNaN(+str2)) return false;
  if(str1.length === 0) return (+str2 + 0).toString();
  if(str2.length === 0) return (+str1 + 0).toString();
  else {
    return (+str1 + +str2).toString();
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
	const posts = listOfPosts.filter(e => e.author === authorName).length;
	let countComms = 0;
	listOfPosts.forEach(e => {
		if (e.hasOwnProperty('comments')){
			return countComms += e.comments.filter(el => el.author === authorName).length;
		}
	});
	return `Post:${posts},comments:${countComms}`;
};

const tickets = (people) => {
	let payback = [];
	let fail = false;
	people.forEach((e) => {
		if (+e === 25) {
			payback.push(e);
			return;
		}
		if (+e === 50 && payback.includes(25)) {
			payback.push(e);
			payback.splice(payback.indexOf(25), 1);
			return;
		} else if (+e === 100 && payback.includes(25) && payback.includes(50)) {
			payback.push(e);
			payback.splice(payback.indexOf(25), 1);
			payback.splice(payback.indexOf(50), 1);
			return;
		} else {
			fail = true;
		}
	});
	if (fail) return 'NO';
	return 'YES';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
